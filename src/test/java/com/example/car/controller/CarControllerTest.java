package com.example.car.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.example.car.model.Car;
import com.example.car.service.CarService;

@RunWith(SpringRunner.class)
@WebMvcTest()
public class CarControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private CarService carService;

	@Test
	public void testGetCarById() throws Exception {
		var car = new Car();
		car.setId(1L);
		car.setMake("BMW");

		when(carService.retrieveById(1L)).thenReturn(car);

		mvc.perform(get("/car/1").contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("{\"make\":\"BMW\",\"id\":1}"));

		verify(carService).retrieveById(1L);
	}

	@Test
	public void testGetAllCars() throws Exception {
		var car1 = new Car();
		car1.setId(1L);
		car1.setMake("BMW");

		var car2 = new Car();
		car2.setId(2L);
		car2.setMake("Audi");

		var cars = List.of(car1, car2);

		when(carService.retrieveAll()).thenReturn(cars);

		mvc.perform(get("/car").contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("[{\"make\":\"BMW\",\"id\":1},{\"make\":\"Audi\",\"id\":2}]"));

		verify(carService).retrieveAll();
	}

	@Test
	public void testPostCar() throws Exception {
		var car = new Car();
		car.setMake("BMW");

		when(carService.createCar(any(Car.class))).thenReturn(1L);

		mvc.perform(post("/car").contentType(MediaType.APPLICATION_JSON).content("{\"make\":\"BMW\"}")).andDo(print())
				.andExpect(status().isCreated());

		verify(carService).createCar(any(Car.class));
	}

	@Test
	public void testPutCar() throws Exception {
		mvc.perform(put("/car").contentType(MediaType.APPLICATION_JSON).content("{\"make\":\"BMW\"}")).andDo(print())
				.andExpect(status().isOk());

		verify(carService).updateCar(any(Car.class));
	}

}
