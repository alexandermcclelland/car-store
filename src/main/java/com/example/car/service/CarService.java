package com.example.car.service;

import java.util.List;

import com.example.car.model.Car;

public interface CarService {
	Long createCar(Car car);
	
	Car retrieveById(Long id);
		
	List<Car> retrieveAll();
	
	void updateCar(Car car);
	
	void deleteById(Long id);
}
