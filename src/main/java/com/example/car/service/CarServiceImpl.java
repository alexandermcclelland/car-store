package com.example.car.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.car.model.Car;
import com.example.car.repository.CarRepository;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarRepository carRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(CarServiceImpl.class);

	@Override
	public Long createCar(Car car) {
		logger.info("Creating car: {}", car);
		var created = carRepository.save(car);
		return created.getId();
	}

	@Override
	public Car retrieveById(Long id) {
		logger.info("Retrieving car by id: {}", id);
		var car = carRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Failed to find car with id: " + id));
		return car;
	}

	@Override
	public List<Car> retrieveAll() {
		logger.info("Retrieving all cars");
		var cars = carRepository.findAll();
		return cars;
	}

	@Override
	public void updateCar(Car car) {
		logger.info("Updating car: {}", car);
		var existing = retrieveById(car.getId());
		existing.setMake(car.getMake());
		carRepository.save(existing);
	}

	@Override
	public void deleteById(Long id) {
		logger.info("Deleting car by id: {}", id);
		carRepository.deleteById(id);
	}
}
