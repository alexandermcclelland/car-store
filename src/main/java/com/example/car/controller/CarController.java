package com.example.car.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.car.model.Car;
import com.example.car.service.CarService;

@RestController
public class CarController {

	@Autowired
	private CarService carService;

	private static final Logger logger = LoggerFactory.getLogger(CarController.class);

	@GetMapping("/car/{id}")
	public ResponseEntity<Car> getCarById(@PathVariable Long id) {
		logger.info("Getting car by id: {}", id);
		var car = carService.retrieveById(id);
		return new ResponseEntity<>(car, HttpStatus.OK);
	}

	@GetMapping("/car")
	public ResponseEntity<List<Car>> getAllCars() {
		logger.info("Getting all cars");
		var cars = carService.retrieveAll();
		return new ResponseEntity<>(cars, HttpStatus.OK);
	}

	@PostMapping("/car")
	public ResponseEntity<Long> postCar(@RequestBody Car car) {
		logger.info("Posting car: {}", car);
		var id = carService.createCar(car);
		return new ResponseEntity<>(id, HttpStatus.CREATED);
	}

	@PutMapping("/car")
	public ResponseEntity<Void> putCar(@RequestBody Car car) {
		logger.info("Putting car: {}", car);
		carService.updateCar(car);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/car/{id}")
	public ResponseEntity<Void> deleteCarById(@PathVariable Long id) {
		logger.info("Deleting car by id: {}", id);
		carService.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
